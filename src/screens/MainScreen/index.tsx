import React, { FunctionComponent } from 'react'
import Layout from './Layout'

const MainScreen: FunctionComponent = () => {
    return <Layout />
}

export default MainScreen