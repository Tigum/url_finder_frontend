import React, { FunctionComponent } from 'react'
import { RouteType } from './interfaces'
import { Switch, Route } from 'react-router-dom'
import MainScreen from '../screens/MainScreen'
import ResultsScreen from '../screens/ResultsScreen'

const Routes: FunctionComponent = () => {

    const routes: RouteType[] = [
        { path: '/', component: <MainScreen /> },
        { path: '/results', component: <ResultsScreen /> }
    ]

    return <Switch>
        {routes.map((route: RouteType, i: number) => <Route path={route.path} children={route.component} key={i} />)}
    </Switch>


}

export default Routes